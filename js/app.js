
console.clear();
var audioCtx = new AudioContext();
var audioCtx = new (window.AudioContext || window.webkitAudioContext)();

// récupérons nos cases à cocher dans l'interface.
// Nous voulons les garder dans les groupes
// dans lesquels ils se trouvent car chaque ligne représente un son ou une voix différent.

const pads = document.querySelectorAll('.pads');
const allPadButtons = document.querySelectorAll('#tracks button');
//  changer d'attribut aria au clic
allPadButtons.forEach(el => {
  el.addEventListener('click', () => {
    if (el.getAttribute('aria-checked') === 'false') {
      el.setAttribute('aria-checked', 'true');
    } else {
      el.setAttribute('aria-checked', 'false');
    }
  }, false)
})

// Son
// ──────────────────────────────────────────────────

var sonPattern1 = './Sound/Dnb/Kick/kick_1.wav';
var sonPattern2 = './Sound/Dnb/HH/HiHat-Closed-1.wav';
var sonPattern3 = './Sound/Dnb/HH/HiHat-Open-1.wav';
var sonPattern4 = './Sound/Dnb/Snare/Snare_2.wav';
var sonPattern5 = './Sound/Dnb/Bass/Bass-Cm-1.wav';

// Rate
//----------------------------------------------------

var playbackControl = document.getElementById ('playback-rate-control');
var playbackValue = document.getElementById ('playback-rate-value');
var playbackControl1 = document.getElementById ('playback-rate-control-1');
var playbackValue1 = document.getElementById ('playback-rate-value-1');
var playbackControl2 = document.getElementById ('playback-rate-control-2');
var playbackValue2 = document.getElementById ('playback-rate-value-2');
var playbackControl3 = document.getElementById ('playback-rate-control-3');
var playbackValue3 = document.getElementById ('playback-rate-value-3');
var playbackControl4 = document.getElementById ('playback-rate-control-4');
var playbackValue4 = document.getElementById ('playback-rate-value-4');

// Volume
// ----------------------------------------------------

var volumeControl = document.getElementById ('volume-control');
var volumeValue = document.getElementById ('volume-value');
var volumeControl1 = document.getElementById ('volume-control-1');
var volumeValue1 = document.getElementById ('volume-value-1');
var volumeControl2 = document.getElementById ('volume-control-2');
var volumeValue2 = document.getElementById ('volume-value-2');
var volumeControl3 = document.getElementById ('volume-control-3');
var volumeValue3 = document.getElementById ('volume-value-3');
var volumeControl4 = document.getElementById ('volume-control-4');
var volumeValue4 = document.getElementById ('volume-value-4');

// Loading ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// récupérer le fichier audio et décoder les données

// Nous voulons nous assurer que notre fichier
// a été chargé et décodé dans un tampon avant de l'utiliser,
// créons donc une fonction asynchrone pour nous permettre de le faire:

async function getFile(audioContext, filepath) {
  const response = await fetch(filepath);
  const arrayBuffer = await response.arrayBuffer();
  const audioBuffer = await audioContext.decodeAudioData(arrayBuffer);
  return audioBuffer;
}

// Nous pouvons ensuite utiliser l' await opérateur lors de l'appel de cette fonction,
// ce qui garantit que nous ne pourrons exécuter le code suivant qu'une fois l'exécution terminée.

// Créons une autre fonction asynchrone pour configurer l'exemple.
// Nous pouvons combiner les deux fonctions asynchrones dans un joli modèle de promesse
// pour effectuer d'autres actions lorsque ce fichier est chargé et mis en mémoire tampon:

/*
var kick =  { "kick-1": './Sound/Dnb/Kick/kick_1.wav',
              "kick-2": './Sound/Dnb/Kick/kick-2.wav'

};
*/
/*
var sample = [
  {kick-1 : './Sound/Dnb/Kick/kick_1.wav',
   kick-2 : './Sound/Dnb/Kick/kick-2.wav'},
  {}]
*/
async function setupSample(index, path) {
    let defaultValues = [ sonPattern1, // Je met des variables dans mon tableau pour le changement de son via le onChange sinon quand je change le son sur deux pattern differentes il reinitialise les autre pattern a cause du setupSongs.
                          sonPattern2,
                          sonPattern3,
                          sonPattern4,
                          './Sound/Dnb/Bass/Bass-Cm-1.wav'
    ];

    if (path) {
      defaultValues[index] = path;
    }

    let filePath = defaultValues;

    console.log(filePath);
    var samples = [];
    for ( let i= 0; i < filePath.length; i++) {
      samples[i] = await getFile(audioCtx, filePath[i]);
          }
    console.log(samples);
    return samples;
}
//console.log(setupSample());

function changeDefaultValues(){

}

// lorsque l'échantillon est chargé, autoriser la lecture

const loadingEl = document.querySelector('.loading');
const playButton = document.querySelector('#play');
const stopButton = document.querySelector('#stop');
const resumeButton = document.querySelector('#resume');



let isPlaying = false;

function setupSongs(index = null, path = null) {
  setupSample(index, path)
    .then((samples) => {
      loadingEl.style.display = 'none'; // si la promesse est resolue on n'affiche plus le loading
      TableauSamples = samples; // à utiliser dans notre fonction playSample

    });


}

setupSongs();

playButton.addEventListener('click', ev => {

    currentNote = 0;
    nextNoteTime = audioCtx.currentTime;
    scheduler(); // lancement de la planification
    requestAnimationFrame(draw); // démarrez la boucle de dessin.
    //ev.target.dataset.playing = 'true';

})

stopButton.addEventListener('click', ev=> {
  //audioCtx.close()
  window.clearTimeout(timerID);
  playButton.setAttribute('data-playing', 'false');

});

resumeButton.addEventListener('click', ev=> {

    audioCtx.resume();
    currentNote = 0;
    requestAnimationFrame(draw);

});


//-----------------------------------------------------

// créer un tampon, insérer des données,
// connecter et jouer -> modifier le graphe ici si nécessaire

function playSample(audioContext, audioBuffer) {

  let sampleSource = audioContext.createBufferSource();
  sampleSource.buffer = audioBuffer;
  gainNode = audioCtx.createGain();
  sampleSource.connect(gainNode).connect(audioContext.destination); //JE PENSE QUE JE DOIS METTRE UNE BOUCLE ICI POUR CONNECTER MES NODES ET FAIRE EN SORTE DE NE PAS AVOIR
  sampleSource.start();
  return sampleSource;
}

// ------------------------------------------------------------------
// Pour changer la valeur du playbackratevalue.
/*
playbackControl.oninput = function () {
  sampleSource.playbackRate.value = playbackControl.value;
  playbackValue.innerHTML = playbackControl.value;
}
*/



// Scheduling ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~(Planification)
let tempo = 120.0;
const bpmControl = document.querySelector('#bpm');
const bpmValEl = document.querySelector('#bpmval');
bpmControl.addEventListener('input', ev => {
  tempo = Number(ev.target.value);
  bpmValEl.innerText = tempo;
}, false);
const lookahead = 5.0; // À quelle fréquence appeler la fonction de planification - How frequently to call scheduling function (in milliseconds)
const scheduleAheadTime = 0.1; // Combien de temps à l'avance pour planifier l'audio - How far ahead to schedule audio (sec)
let currentNote = 0; // La note que nous jouons actuellement
let nextNoteTime = 0.0; // when the next note is due.
function nextNote() {
  const secondsPerBeat = 15 / tempo; // ICI POUR REGLER LA VITESSE DE LECTURE vu que tempo 60 = 60 sec si divise par tempo vitesse de defilement plus rapide.
  nextNoteTime += secondsPerBeat; // Add beat length to last beat time
  // Advance the beat number, wrap to zero
  currentNote++;
  if (currentNote === 32) { // ICI POUR CHANGER LE NOMBRE DE CASE
    currentNote = 0;
  }
}


// Create a queue for the notes that are to be played, with the current time that we want them to play:
// Créez une file d’attente pour les notes à jouer, avec l’heure à laquelle nous voulons qu’elles soient jouées:
const notesInQueue = [];
let TableauSamples;




function scheduleNote(beatNumber, time) {
  // push the note on the queue, even if we're not playing.
  notesInQueue.push({note: beatNumber, time: time});
  // console.log(beatNumber, time);
  if (pads[0].querySelectorAll('button')[currentNote].getAttribute('aria-checked') === 'true') {
    //playSample(audioCtx, TableauSamples[0]);
//gainNode = audioCtx.createGain();

      playSample(audioCtx, TableauSamples[0]).playbackRate.setValueAtTime(playbackControl.value, audioCtx.currentTime);
      gainNode.gain.value=volumeControl.value
  }
  if (pads[1].querySelectorAll('button')[currentNote].getAttribute('aria-checked') === 'true') {
      playSample(audioCtx, TableauSamples[1]).playbackRate.setValueAtTime(playbackControl1.value, audioCtx.currentTime);
      gainNode.gain.value=volumeControl1.value
  }
  if (pads[2].querySelectorAll('button')[currentNote].getAttribute('aria-checked') === 'true') {
      playSample(audioCtx, TableauSamples[2]).playbackRate.setValueAtTime(playbackControl2.value, audioCtx.currentTime);
      gainNode.gain.value=volumeControl2.value
  }
  if (pads[3].querySelectorAll('button')[currentNote].getAttribute('aria-checked') === 'true') {
      playSample(audioCtx, TableauSamples[3]).playbackRate.setValueAtTime(playbackControl3.value, audioCtx.currentTime);
      gainNode.gain.value=volumeControl3.value
  }
  if (pads[4].querySelectorAll('button')[currentNote].getAttribute('aria-checked') === 'true') {
      playSample(audioCtx, TableauSamples[4]).playbackRate.setValueAtTime(playbackControl4.value, audioCtx.currentTime);
      gainNode.gain.value=volumeControl4.value
  }

}
let timerID;
function scheduler() {
  // alors qu'il y a des notes qui devront être jouées avant le prochain intervalle -
  // programmez-les et avancez le pointeur -
  while (nextNoteTime < audioCtx.currentTime + scheduleAheadTime ) {
      scheduleNote(currentNote, nextNoteTime);
      nextNote();
  }
  timerID = window.setTimeout(scheduler, lookahead);
}
// Nous avons également besoin d'une fonction de dessin pour mettre à jour l'interface utilisateur,
// afin de pouvoir voir quand le battement progresse.
let lastNoteDrawn = 3;
function draw() {
  let drawNote = lastNoteDrawn;
  const currentTime = audioCtx.currentTime;
  while (notesInQueue.length && notesInQueue[0].time < currentTime) {
    drawNote = notesInQueue[0].note;
    notesInQueue.splice(0,1);   // remove note from queue
  }
  // We only need to draw if the note has moved.
  if (lastNoteDrawn !== drawNote) {
    pads.forEach(el => {
      el.children[lastNoteDrawn].style.borderColor = 'hsla(0, 0%, 10%, 1)';
      el.children[drawNote].style.borderColor = 'hsla(49, 99%, 50%, 1)';
    });
    lastNoteDrawn = drawNote;
  }
  // set up to draw again
  requestAnimationFrame(draw);
}



  // BOUTON INPUT
/*
  $(".rate").roundSlider({
      radius: 30, //40
      width: 8, //16
      min:0.1,
      max:10,
      value:1,
      step:0.2,
      //handleSize: "+16",
      //handleShape: "dot",
      sliderType: "min-range",

  });

  $(".volume").roundSlider({
      radius: 30,
      width: 8,
      min:0,
      max:1.2,
      value:1,
      step:0.1,
      //handleSize: "+16",
      //handleShape: "dot",
      sliderType: "min-range",

  });
*/
