// import { AudioContext } from 'https://dev.jspm.io/standardized-audio-context';
console.clear();
var audioCtx = new AudioContext();
var audioCtx = new (window.AudioContext || window.webkitAudioContext)();
// TODO make all frequencies like each other
// TODO set value at time rather than value
// Before we do anything more, let's grab our checkboxes from the interface. We want to keep them in the groups they are in as each row represents a different sound or _voice_.
const pads = document.querySelectorAll('.pads');
const allPadButtons = document.querySelectorAll('#tracks button');
// switch aria attribute on click
allPadButtons.forEach(el => {
  el.addEventListener('click', () => {
    if (el.getAttribute('aria-checked') === 'false') {
      el.setAttribute('aria-checked', 'true');
    } else {
      el.setAttribute('aria-checked', 'false');
    }
  }, false)
})



// Je m'assure que mon fichier
// a été chargé et décodé dans un tampon avant de l'utiliser,
// je crée une fonction asynchrone pour me permettre de le faire:

async function getSound1(audioContext, sound1Url) {
  const response = await fetch(sound1Url);
  const arrayBuffer = await response.arrayBuffer();
  const audioBuffer = await audioContext.decodeAudioData(arrayBuffer);
  return audioBuffer;
}

// Nous pouvons ensuite utiliser l' await opérateur lors de l'appel de cette fonction,
// ce qui garantit que nous ne pourrons exécuter le code suivant qu'une fois l'exécution terminée.

// Créons une autre fonction asynchrone pour configurer l'exemple.
// Nous pouvons combiner les deux fonctions asynchrones dans un joli modèle de promesse
// pour effectuer d'autres actions lorsque ce fichier est chargé et mis en mémoire tampon:

async function setupSound1() {
    const sound1Url = './Sound/Dnb/Kick/kick_1.wav';
    var sound1  = await getSound1(audioCtx, sound1Url);
    console.log(sound1);
    return sound1;
}


// lorsque l'échantillon est chargé, autoriser la lecture

const loadingEl = document.querySelector('.loading');
const playButton = document.querySelector('[data-playing]');
let isPlaying = false;
setupSample()
  .then((sound1) => {
    loadingEl.style.display = 'none';
    sample1 = sound1; // à utiliser dans notre fonction playSample
    playButton.addEventListener('click' , ev => {
      isPlaying = !isPlaying;
      if (isPlaying) { // start playing
        // check if context is in suspended state (autoplay policy)
        if (audioCtx.state === 'suspended') {
          audioCtx.resume();
        }
        currentNote = 0;
        nextNoteTime = audioCtx.currentTime;
        scheduler(); // kick off scheduling
        requestAnimationFrame(draw); // start the drawing loop.
        ev.target.dataset.playing = 'true';
      } else {
        window.clearTimeout(timerID);
        ev.target.dataset.playing = 'false';
      }
    })
  });

// Rate
//----------------------------------------------------

  var valueOfPlaybackRate = 1;
  const rateControl = document.querySelector('#rate');
  rateControl.addEventListener('input', ev => {
    valueOfPlaybackRate = Number(ev.target.value);
  }, false);


//-----------------------------------------------------


// create a buffer, plop in data, connect and play -> modify graph here if required
function playSound1(audioContext, audioBuffer) {


  const sound1Source = audioContext.createBufferSource();
  sound1Source.buffer = audioBuffer;
  sound1Source.playbackRate.setValueAtTime(valueOfPlaybackRate, audioCtx.currentTime);
  sound1Source.connect(audioContext.destination);
  sound1Source.start();
  return sound1Source;
}




// Scheduling ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~(Planification)
let tempo = 120.0;
const bpmControl = document.querySelector('#bpm');
const bpmValEl = document.querySelector('#bpmval');
bpmControl.addEventListener('input', ev => {
  tempo = Number(ev.target.value);
  bpmValEl.innerText = tempo;
}, false);
const lookahead = 5.0; // À quelle fréquence appeler la fonction de planification - How frequently to call scheduling function (in milliseconds)
const scheduleAheadTime = 0.1; // Combien de temps à l'avance pour planifier l'audio - How far ahead to schedule audio (sec)
let currentNote = 0; // The note we are currently playing
let nextNoteTime = 0.0; // when the next note is due.
function nextNote() {
  const secondsPerBeat = 15 / tempo; // ICI POUR REGLER LA VITESSE DE LECTURE vu que tempo 60 = 60 sec si divise par tempo vitesse de defilement plus rapide.
  nextNoteTime += secondsPerBeat; // Add beat length to last beat time
  // Advance the beat number, wrap to zero
  currentNote++;
  if (currentNote === 64) { // ICI POUR CHANGER LE NOMBRE DE CASE
    currentNote = 0;
  }
}


// Create a queue for the notes that are to be played, with the current time that we want them to play:
// Créez une file d’attente pour les notes à jouer, avec l’heure à laquelle nous voulons qu’elles soient jouées:
const notesInQueue = [];
let sample1;

function scheduleNote(beatNumber, time) {
  // push the note on the queue, even if we're not playing.
  notesInQueue.push({note: beatNumber, time: time});
  // console.log(beatNumber, time);
  if (pads[0].querySelectorAll('button')[currentNote].getAttribute('aria-checked') === 'true') {
    playSound1(audioCtx);

  }
  if (pads[1].querySelectorAll('button')[currentNote].getAttribute('aria-checked') === 'true') {
    playSound1(audioCtx)
  }
  if (pads[2].querySelectorAll('button')[currentNote].getAttribute('aria-checked') === 'true') {
    playSound1(audioCtx)
  }
  if (pads[3].querySelectorAll('button')[currentNote].getAttribute('aria-checked') === 'true') {
    playSound1(audioCtx);
  }
}
let timerID;
function scheduler() {
  // alors qu'il y a des notes qui devront être jouées avant le prochain intervalle - while there are notes that will need to play before the next interval,
  // programmez-les et avancez le pointeur - schedule them and advance the pointer.
  while (nextNoteTime < audioCtx.currentTime + scheduleAheadTime ) {
      scheduleNote(currentNote, nextNoteTime);
      nextNote();
  }
  timerID = window.setTimeout(scheduler, lookahead);
}
// We also need a draw function to update the UI, so we can see when the beat progresses.
let lastNoteDrawn = 3;
function draw() {
  let drawNote = lastNoteDrawn;
  const currentTime = audioCtx.currentTime;
  while (notesInQueue.length && notesInQueue[0].time < currentTime) {
    drawNote = notesInQueue[0].note;
    notesInQueue.splice(0,1);   // remove note from queue
  }
  // We only need to draw if the note has moved.
  if (lastNoteDrawn !== drawNote) {
    pads.forEach(el => {
      el.children[lastNoteDrawn].style.borderColor = 'hsla(0, 0%, 10%, 1)';
      el.children[drawNote].style.borderColor = 'hsla(49, 99%, 50%, 1)';
    });
    lastNoteDrawn = drawNote;
  }
  // set up to draw again
  requestAnimationFrame(draw);
}



  // BOUTON INPUT
/*
  $(".slider").roundSlider({
      radius: 80,
      width: 8,
      //handleSize: "+16",
      //handleShape: "dot",
      sliderType: "min-range",

  });
*/
